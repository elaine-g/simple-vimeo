<?php

/**
 *   @author Rodrigo Maldonado Gasca <elaineeeit@gmail.com>
 *   @license LICENSE-2.0
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

namespace SimpleVimeo;

require_once(__DIR__.'/Exceptions/SimpleVimeoException.php');

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoRequestException;
use Vimeo\Exceptions\VimeoUploadException;
use SimpleVimeo\Exceptions\SimpleVimeoException;

class SimpleVimeo
{

    /**
     * @var integer
     */
    private $clientId;
    /**
     * @var integer
     */
    private $clientSecret;
    /**
     * @var integer
     */
    private $config;

    /**
     * @var integer
     */
    public $video;

    /**
     * @var integer
     */
    private $vimeo;
    /**
     * @var integer
     */
    public $headers;
    /**
     * @var integer
     */
    public $status;

    const REQUEST_GET = "GET";
    const REQUEST_POST = "POST";
    const REQUEST_PUT = "PUT";
    const REQUEST_DELETE = "DELETE";
    const REQUEST_PATCH = "PATCH";

    /**
     * @param array $config an array whos contain private credentials to instanciate vimeo sdk
     * @throws SimpleVimeoException
     */

    public function __construct($config = array())
    {
        if (!is_null($config)) {
            $this->clientId = isset($config['clientId']) ? $config['clientId']: null;
            $this->clientSecret = isset($config['clientSecret']) ? $config['clientSecret'] : null;
            $this->accessToken = isset($config['accessToken']) ? $config['accessToken'] : null;
            if (is_null($this->accessToken) || !$this->accessToken) {
                throw new SimpleVimeoException('You must set an Access Token');
            }
            $this->vimeo = new Vimeo($this->clientId, $this->clientSecret, $this->accessToken);
            return $this;
        }
        throw new SimpleVimeoException('You must set credentials');
    }

    /**
     * @param string $endpoint
     * @param array $fields
     * @param string $method
     */

    private function request($endpoint, $fields = array(), $method)
    {
        if (isset($this->vimeo)) {
            $request = $this->vimeo->request($endpoint, $fields, $method);
            $this->headers = $request['headers'];
            $this->status = $request['status'];
            return $request['body'];
        }
        throw new SimpleVimeoException('Vimeo sdk is not instanced yet');
    }

    /**
     * Get request headers
     *
     * @return array|null
     */

    public function getRequestHeaders()
    {
        return isset($this->headers) ? $this->headers: null;
    }

    /**
     * Get request status
     *
     * @return array|null
     */

    public function getRequestStatus()
    {
        return isset($this->status) ? $this->status : null;
    }

    /**
     * Get an video from their id
     *
     * @param integer $videoId video id
     * @param array $fields vimeo sdk fields
     * @throws SimpleVimeoException
     * @return array
     */
    
    public function getVideoById($videoId = null, $fields= array())
    {
        if (!$videoId) {
            throw new SimpleVimeoException('Invalid video id');
        }
        $endpoint = '/videos/%s';
        $fields = !empty($fields) ? '?fields='.implode(',', $fields) : '';
        return $this->request(sprintf($endpoint.$fields, $videoId), array(), self::REQUEST_GET);
    }

    /**
     * Edit an video by their id
     *
     * @param integer $videoId
     * @param array $body
     * @return array
     */

    public function editVideo($videoId, $body  = array())
    {
        $endpoint = '/videos/%s';
        return $this->request(sprintf($endpoint, $videoId), $body, self::REQUEST_PATCH);
    }

    /**
     * Deletes and video by their id
     *
     * @param integer $videoId
     * @return array
     */

    public function deleteVideo($videoId)
    {
        $endpoint = '/videos/%s';
        return $this->request(sprintf($endpoint, $videoId), array(), self::REQUEST_DELETE);
    }

    /**
     * Replace video
     *
     * @param string $videoURI video uri to replace
     * @param integer $videoPath new video path
     * @param array $params extra parameters
     * @return array
     */

    public function replaceVideo($videoURI, $videoPath, $params = array())
    {
        return $this->vimeo->replace($videoURI, $videoPath, $params);
    }

    /**
     * Get all videos from an account
     *
     * @param array $filters
     * @return array
     */

    public function getAllVideos($filters = array())
    {
        $endpoint = '/me/videos';
        return $this->request($endpoint, $filters, self::REQUEST_GET);
    }

    /**
     * Get all embed presets
     *
     * @param integer $page page embed
     * @param integer $perPage max results per page
     * @return array
     */

    public function getAllEmbedPresets($page = 1, $perPage = 10)
    {
        $endpoint = '/me/presets?page=%s&per_page=%s';
        return $this->request(sprintf($endpoint, $page, $perPage), array(), self::REQUEST_GET);
    }

    /**
     * Get embed preset by name
     *
     * @param string $itemKey embed preset name
     * return array|string
     */

    public function getEmbedPreset($presetName)
    {
        $data = self::getAllEmbedPresets();
        foreach ($data['data'] as $key => $value) {
            if ($data['data'][$key]['name'] === $presetName) {
                return $data['data'][$key];
                break;
            }
        }
    }
    
    /**
     * Get video domains embeded
     *
     * @param integer $videoId
     * @param integer $page
     * @param integer $perPage
     * @return array
     */

    public function getVideoDomainsEmbedded($videoId, $page = 1, $perPage=10)
    {
        $endpoint = '/videos/%s/privacy/domains?page=%s&per_page=%s';
        return $this->request(sprintf($endpoint, $videoId, $page, $perPage), array(), self::REQUEST_GET);
    }

    /**
     * Change video privacy domain
     *
     * @param integer $videoId
     * @param string $domain
     * @return array
     */

    public function changeVideoPrivacyDomain($videoId, $domain)
    {
        $endpoint = '/videos/%s/privacy/domains/%s';
        return $this->request(sprintf($endpoint, $videoId, $domain), array(), self::REQUEST_PUT);
    }

    /**
     * Upload an video from file path
     *
     * @param string $filepath
     * @param array $settings
     * @return object
     */

    public function upload($filepath, $settings = array())
    {
        if (is_null($settings) || empty($settings)) {
            throw new SimpleVimeoException('You must set settings parameter.');
        }
        $this->video = $this->vimeo->upload($filepath, $settings);
        return $this;
    }

    /**
     * Upload  video file from uri
     *
     * @param string $url
     * @param array $body
     * @return object Request upload video
     */

    public function uploadByURI($url, $body = array())
    {
        if (!isset($url) || empty($url)) {
            throw new SimpleVimeoException('Invalid video url.');
        }
        $endpoint = '/me/videos';
        $body = !empty($body) ? $body : array('upload' => array('approach' => 'pull','link' => $url));
        $this->video = $this->request($endpoint, $body, self::REQUEST_POST);
        return $this;
    }

    /**
     * Get current video status.
     *
     * @param array $fitlers
     * @return array
     * @throws SimpleVimeoException
     */

    public function getCurrentVideoStatus($filters = array())
    {
        if (isset($this->video)) {
            $videoId = @isset(explode('/', $this->video)[2]) ? explode('/', $this->video)[2] : end(explode('/', $this->video['uri']));
            $endpoint = '/videos/%s';
            $filters = !empty($filters) ? '?fields='.implode(',', $filters) : '';
            if ($videoId) {
                return $this->request(sprintf($endpoint, $videoId).$filters, array(), self::REQUEST_GET);
            }
        } else {
            throw new SimpleVimeoException('No video to process');
        }
    }

    /**
     * Get current video id.
     *
     * @return string|integer
     * @throws SimpleVimeoException
     */

    public function getCurrentVideoId()
    {
        if (isset($this->video)) {
            $video =  @isset(explode('/', $this->video)[2]) ? explode('/', $this->video)[2] : false;
            if (!$video) {
                $videoURI = explode('/',$this->video['uri']);
                $videoId= end($videoURI);
                if ($videoId) {
                    return $videoId;
                }
            }
            if ($video !== false) {
                return $video;
            }
            throw new SimpleVimeoException('Cannot get current video id');
        }
        throw new SimpleVimeoException('Not current video is processing');
    }
}
